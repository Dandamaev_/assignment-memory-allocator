//
// Created by forkjoin on 12.03.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

#include <stdbool.h>
#include <stddef.h>

void test__malloc_Given_oneBlock_blockSize30_Expected_nonFree_capacity30(void const *heap);

void
test__malloc_Given_threeBlocks_blockSize30_Free_thirdBlock_Expected_firstBlock_nonFree_secondBlock_nonFree_thirdBlock_free(
        void const *heap);

void
test__malloc_Given_threeBlocks_blockSize20_30_35_Free_secondBlock_thirdBlock_Expected_firstBlock_nonFree_secondBlock_free_thirdBlock_free(
        void const *heap);

void
test__malloc_Given_oneBlock_blockSize5heapSize_Expected_nonFree_capacity5heapSize(void const *heap, size_t heap_size);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
