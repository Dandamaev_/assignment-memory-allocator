//
// Created by forkjoin on 9.03.2022.
//

#include "mem.h"
#include "test.h"
#include "util.h"
#include <stdio.h>


int main() {

    printf("Heap initialization!\n");

    size_t heap_size = 5000;

    void *heap = heap_init(heap_size);

    if (NULL == heap)
        err("Heap initialization error!\n");

    debug_heap(stdout, heap);

    test__malloc_Given_oneBlock_blockSize30_Expected_nonFree_capacity30(heap);

    test__malloc_Given_threeBlocks_blockSize30_Free_thirdBlock_Expected_firstBlock_nonFree_secondBlock_nonFree_thirdBlock_free(
            heap);

    test__malloc_Given_threeBlocks_blockSize20_30_35_Free_secondBlock_thirdBlock_Expected_firstBlock_nonFree_secondBlock_free_thirdBlock_free(
            heap);

    test__malloc_Given_oneBlock_blockSize5heapSize_Expected_nonFree_capacity5heapSize(heap, heap_size);

}
